package io.treev.model

import io.treev.util._, tags._, mechanics._
import scala.language.higherKinds
import scalaz.{Free, Inject}

object logging {

  /** Logging model marker trait. */
  trait Logging

  /** Base business logging action. */
  sealed trait LoggingAction[+A] extends Action[A] {
    /** Logging message. */
    def msg: String
    /** Logging message source. */
    def source: String
    /** Optional cause. */
    def cause: Option[Throwable]
    /** This logging action logging level. */
    def level: Level
  }
  object LoggingAction {
    /** Log error. */
    case class Error(override val msg: String,
                     override val source: String,
                     override val cause: Option[Throwable]) extends LoggingAction[Unit] {
      override def level: Level = Level.Error
    }
    /** Log warning. */
    case class Warn(override val msg: String,
                    override val source: String,
                    override val cause: Option[Throwable]) extends LoggingAction[Unit] {
      override def level: Level = Level.Warn
    }
    /** Log information. */
    case class Info(override val msg: String,
                    override val source: String,
                    override val cause: Option[Throwable]) extends LoggingAction[Unit] {
      override def level: Level = Level.Info
    }
    /** Log debug. */
    case class Debug(override val msg: String,
                     override val source: String,
                     override val cause: Option[Throwable]) extends LoggingAction[Unit] {
      override def level: Level = Level.Debug
    }

    /** Create logging action. */
    def apply(msg: String, source: String, level: Level, cause: Option[Throwable]): LoggingAction[Unit] =
      level match {
        case Level.Error ⇒ Error(msg, source, cause)
        case Level.Warn ⇒ Warn(msg, source, cause)
        case Level.Info ⇒ Info(msg, source, cause)
        case Level.Debug ⇒ Debug(msg, source, cause)
      }
  }

  /** Logging level. */
  sealed trait Level extends enumeratum.EnumEntry
  object Level extends enumeratum.Enum[Level] {
    override def values: Seq[Level] = findValues

    case object Error extends Level
    case object Warn extends Level
    case object Info extends Level
    case object Debug extends Level
  }

  /** Free interface to logging instructions. */
  class LoggingActions[F[_]](implicit I: Inject[LoggingAction, F]) {
    import LoggingAction._

    def error(msg: String, cause: Option[Throwable] = None)(implicit source: String @@ Logging): Free[F, Unit] =
      lift(Error(msg, source, cause))
    def warn(msg: String, cause: Option[Throwable] = None)(implicit source: String @@ Logging): Free[F, Unit] =
      lift(Warn(msg, source, cause))
    def info(msg: String, cause: Option[Throwable] = None)(implicit source: String @@ Logging): Free[F, Unit] =
      lift(Info(msg, source, cause))
    def debug(msg: String, cause: Option[Throwable] = None)(implicit source: String @@ Logging): Free[F, Unit] =
      lift(Debug(msg, source, cause))

    def log(msg: String, source: String, level: Level = Level.Info, cause: Option[Throwable] = None): Free[F, Unit] =
      lift(LoggingAction(msg, source, level, cause))
  }
  object LoggingActions {
    implicit def instance[F[_]](implicit I: Inject[LoggingAction, F]): LoggingActions[F] =
      new LoggingActions[F]
  }

}
