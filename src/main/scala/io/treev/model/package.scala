package io.treev

import scala.language.implicitConversions

package object model {

  /** Base action.
    * @tparam A action result type */
  trait Action[+A]

  /** Base application-level error. */
  trait Error
  /** Base programming error. */
  trait ProgrammingError extends Error

  /** Generic error type that wraps an exception. */
  trait GenericError extends Error {
    /** Exception that caused this error to happen. */
    def cause: Throwable
  }
  object GenericError {
    /** Implicit conversion from a `Throwable`. */
    implicit def fromThrowable(t: Throwable): GenericError =
      new GenericError {
        override def cause: Throwable = t
      }
  }

}
