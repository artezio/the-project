package io.treev.model

import io.treev.util.tags._

object db {

  /** Base db error. */
  trait DbError extends Error
  /** Base db programming error. */
  trait DbProgrammingError extends DbError with ProgrammingError
  /** Generic db error. */
  case class GenericDbError(override val cause: Throwable) extends DbError with GenericError

  /** Base db entity.
    * @tparam I raw identifier type
    * @tparam T entity tag type
    * @tparam D data type */
  case class Entity[I, T, +D](label: String @@ T, id: I @@ T, data: D) {
    type Label = String @@ T
    type Id = I @@ T
    type Data <: D
  }

}
