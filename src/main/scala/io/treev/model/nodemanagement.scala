package io.treev.model

import io.treev.model.db.Entity
import io.treev.util.mechanics._
import io.treev.util.tags._

import scala.language.higherKinds
import scalaz.{Free, Inject}

object nodemanagement {

  /* Tree node. */

  sealed trait TreeNode
  object TreeNode { val Label = "TreeNode".@@[TreeNode] }
  case class TreeNodeData(parentId: Long @@ TreeNode, content: String)
  type TreeNodeEntity = Entity[Long, TreeNode, TreeNodeData]

  /** Base node management generation action. */
  sealed trait NodeManagementAction[+A] extends Action[A]
  object NodeManagementAction {
    /** Create tree node. */
    case class Create(node: TreeNodeEntity) extends NodeManagementAction[Unit]
    /** Read tree node. */
    case class Read(id: TreeNodeEntity#Id) extends NodeManagementAction[Option[TreeNodeEntity]]
  }

  /** Free interface to node management instructions. */
  class NodeManagementActions[F[_]](implicit I: Inject[NodeManagementAction, F]) {
    import NodeManagementAction._

    def create(node: TreeNodeEntity): Free[F, Unit] = lift(Create(node))
    def read(id: TreeNodeEntity#Id): Free[F, Option[TreeNodeEntity]] = lift(Read(id))
  }
  object NodeManagementActions {
    implicit def instance[F[_]](implicit I: Inject[NodeManagementAction, F]): NodeManagementActions[F] =
      new NodeManagementActions[F]
  }

}
