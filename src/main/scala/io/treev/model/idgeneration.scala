package io.treev.model

import io.treev.util.mechanics._

import scala.language.higherKinds
import scalaz.{Free, Inject}

object idgeneration {

  /** Base id generation action. */
  sealed trait IdGenerationAction[+A] extends Action[A]
  object IdGenerationAction {
    /** Generate globally unique identifier. */
    case object GenerateGloballyUnique extends IdGenerationAction[Long]
    /** Generate namespace unique identifier. */
    case class GenerateNamespaceUnique(namespace: String) extends IdGenerationAction[Long]
  }

  /** Free interface to id generation instructions. */
  class IdGenerationActions[F[_]](implicit I: Inject[IdGenerationAction, F]) {
    import IdGenerationAction._

    def generateGloballyUnique(): Free[F, Long] = lift(GenerateGloballyUnique)
    def generateNamespaceUnique(namespace: String): Free[F, Long] = lift(GenerateNamespaceUnique(namespace))
  }
  object IdGenerationActions {
    implicit def instance[F[_]](implicit I: Inject[IdGenerationAction, F]): IdGenerationActions[F] =
      new IdGenerationActions[F]
  }

}
