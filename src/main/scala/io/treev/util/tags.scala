package io.treev.util

import scala.language.higherKinds

object tags {

  /** Tag of type `U`. */
  type Tag[+U] = { type Tag <: U }
  /** Type `T` tagged with tag of type `U`. */
  type @@[+T, +U] = T with Tag[U]

  implicit class Taggable[T](val t: T) extends AnyVal {
    /** Tag with type `U`. */
    def tag[U]: T @@ U = t.asInstanceOf[T @@ U]
    /** Tag with type `U`. */
    def @@[U]: T @@ U = tag[U]

    /** Tag as tagged type `U`. */
    def tagAs[U <: T @@ _]: U = t.asInstanceOf[U]
  }

  implicit class TaggableM[M[_], T](val mt: M[T]) extends AnyVal {
    /** Tag container value(s) with type `U`. */
    def tagM[U]: M[T @@ U] = mt.asInstanceOf[M[T @@ U]]
    /** Tag container value(s) as tagged type `U`. */
    def tagAs[U <: T @@ _]: M[U] = mt.asInstanceOf[M[U]]
  }

  implicit class AndTaggable[T, U](val t: T @@ U) extends AnyVal {
    /** Add tag of type `V` (product tag type). */
    def andTag[V]: T @@ (U with V) = t.asInstanceOf[T @@ (U with V)]
  }

}
