package io.treev.util

object defaulttypeparams {

  /** Encodes default type parameter.
    * @tparam T a type
    * @tparam D default type for a type */
  trait DefaultsTo[T, D]

  object DefaultsTo {
    implicit def defaultDefaultsTo[T]: DefaultsTo[T, T] = null
    implicit def fallback[T, D]: DefaultsTo[T, D] = null
  }

}
