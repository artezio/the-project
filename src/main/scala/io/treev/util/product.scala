package io.treev.util

object product {

  /** Product of `left` and `right` values. */
  case class &[L, R](left: L, right: R)

  implicit class AndPimp[L](val left: L) extends AnyVal {
    /** Make a product of `this` (as left) and `right`. */
    def &[R](right: R): L & R = new &(left, right)
  }

  /* Updater. */

  /** Product updater able to update value of type `A`. */
  trait ProductUpdater[P, A] {
    /** Update product value of type `A`.
      * @return updated product */
    def update(product: P, f: A ⇒ A): P
  }

  trait LowPriorityProductUpdater {
    /** Non-product value updater. */
    implicit def valueUpdater[A]: ProductUpdater[A, A] = new ProductUpdater[A, A] {
      override def update(product: A, f: A ⇒ A): A = f(product)
    }
  }

  object ProductUpdater extends LowPriorityProductUpdater {
    /** Left-biased product value updater. */
    implicit def leftProductUpdater[L, R, A](implicit leftUpdater: ProductUpdater[L, A]): ProductUpdater[L & R, A] =
      new ProductUpdater[L & R, A] {
        override def update(product: L & R, f: A ⇒ A): L & R =
          leftUpdater.update(product.left, f) & product.right
      }

    /** Right-biased product value updater. */
    implicit def rightProductUpdater[L, R, A](implicit rightUpdater: ProductUpdater[R, A]): ProductUpdater[L & R, A] =
      new ProductUpdater[L & R, A] {
        override def update(product: L & R, f: A ⇒ A): L & R =
          product.left & rightUpdater.update(product.right, f)
      }
  }

  /** Update product value of type `A` with function `f`.
    * Won't compile if product contains multiple `A` values.
    * @return updated product */
  def update[P, A](product: P)(f: A ⇒ A)(implicit updater: ProductUpdater[P, A]): P =
    updater.update(product, f)

  /* Reader. */

  /** Product reader able to read value of type `A`. */
  trait ProductReader[P, A] {
    /** Read product value of type `A`. */
    def read(product: P): A
  }

  trait LowPriorityProductReader {
    /** Non-product value reader. */
    implicit def valueReader[A]: ProductReader[A, A] = new ProductReader[A, A] {
      override def read(product: A): A = product
    }
  }

  object ProductReader extends LowPriorityProductReader {
    /** Left-biased product value reader. */
    implicit def leftProductReader[L, R, A](implicit leftReader: ProductReader[L, A]): ProductReader[L & R, A] =
      new ProductReader[L & R, A] {
        override def read(product: L & R): A =
          leftReader.read(product.left)
      }

    /** Right-biased product value reader. */
    implicit def rightProductReader[L, R, A](implicit rightReader: ProductReader[R, A]): ProductReader[L & R, A] =
      new ProductReader[L & R, A] {
        override def read(product: L & R): A =
          rightReader.read(product.right)
      }
  }

  /** Read product value of type `A`.
    * Won't compile if product contains multiple `A` values.
    * @return value of type `A` */
  def read[P, A](product: P)(implicit productReader: ProductReader[P, A]): A =
    productReader.read(product)

}
