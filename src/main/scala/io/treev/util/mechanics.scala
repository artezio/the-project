package io.treev.util

import scala.language.{higherKinds, implicitConversions, reflectiveCalls}
import scalaz._

object mechanics {

  /** Implicit lifting of any single-argument type constructor into `Free`. */
  implicit def lift[F[_], G[_], A](fa: F[A])(implicit I: Inject[F, G]): Free[G, A] =
    Free.liftF(I.inj(fa))

  /** Implicit `or` on natural transformations. */
  implicit class NaturalTransformationOrPimp[F[_], G[_]](fg: F ~> G) {
    /** Create a coproduct from two natural transformations. */
    def or[H[_]](f: H ~> G): ({ type f[x] = Coproduct[F, H, x] })#f ~> G =
      new (({ type f[x] = Coproduct[F, H, x] })#f ~> G) {
        override def apply[A](c: Coproduct[F, H, A]): G[A] =
          c.run.fold[G[A]](fg.apply, f)
      }
  }

}
