package io.treev.util

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal
import scala.util.{Failure, Success}
import scalaz.concurrent.Task
import scalaz.{-\/, \/-}

object concurrency {

  /** Wrap Scala future into Scalaz task without actually running it. */
  def task[T](future: ⇒ Future[T])(implicit ec: ExecutionContext): Task[T] =
    Task.async {
      register ⇒
        future.onComplete {
          case Success(v) => register(\/-(v))
          case Failure(NonFatal(t)) => register(-\/(t))
        }
    }

}
