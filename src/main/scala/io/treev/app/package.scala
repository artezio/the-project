package io.treev

import io.treev.interpreter._
import io.treev.model.db.Entity
import io.treev.model.idgeneration.{IdGenerationAction, IdGenerationActions}
import io.treev.model.logging.{Logging, LoggingAction, LoggingActions}
import io.treev.model.nodemanagement._
import io.treev.util.mechanics._
import io.treev.util.tags._

import scala.language.higherKinds
import scalaz.{Coproduct, Free, Monad, ~>}

package object app {

  /* Application instruction set. */
  type IS0[A] = Coproduct[IdGenerationAction, NodeManagementAction, A]
  type IS[A] = Coproduct[LoggingAction, IS0, A]

  /** Application structure. */
  def structure[IS[_]](implicit Logging: LoggingActions[IS],
                                IdGeneration: IdGenerationActions[IS],
                                NodeManagement: NodeManagementActions[IS]): Free[IS, Unit] = {
    implicit val loggingSource = "root".@@[Logging]

    val rootNodeId = 0L.@@[TreeNode]

    def mkTreeNodeEntity(id: Long @@ TreeNode): TreeNodeEntity =
      Entity(TreeNode.Label, id, TreeNodeData(rootNodeId, "First node, yeah!"))

    for {
      nodeId ← IdGeneration.generateNamespaceUnique("node")
      _ ← Logging.info(s"Node id: $nodeId")
      _ ← NodeManagement.create(mkTreeNodeEntity(nodeId.@@[TreeNode]))
      _ ← Logging.info(s"Created node with id $nodeId")
    } yield ()
  }

  /** Application interpreter. */
  val taskEitherInterpreter: IS ~> TaskEither =
    wiring.loggingInterpreter or
      (wiring.neoIdGenerationInterpreter or wiring.neoNodeManagementInterpreter: IS0 ~> TaskEither)

  /** Interpret application structure (run) in monad `M`. */
  def interpret[IS[_], M[_]](interpreter: IS ~> M)
                            (implicit TargetMonad: Monad[M],
                                      Logging: LoggingActions[IS],
                                      IdGeneration: IdGenerationActions[IS],
                                      NodeManagement: NodeManagementActions[IS]): M[Unit] = {
    structure[IS].foldMap[M](interpreter)
  }

}
