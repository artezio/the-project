package io.treev.app

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import io.treev.interpreter._
import io.treev.interpreter.idgeneration.{IdGenerationApi, _}
import io.treev.interpreter.implicits._
import io.treev.interpreter.logging.{LoggingApi, Slf4jLoggingApi, _}
import io.treev.interpreter.nodemanagement.{NodeManagementApi, _}
import io.treev.model.idgeneration.IdGenerationAction
import io.treev.model.logging.LoggingAction
import io.treev.model.nodemanagement.NodeManagementAction
import io.treev.runtime.RuntimeDeps
import io.treev.support.neo.{NeoConfig, NeoSupportImpl}

import scala.concurrent.ExecutionContext
import scalaz._

object wiring {

  trait RuntimeDepsImpl extends RuntimeDeps {
    override implicit lazy val actorSystem: ActorSystem = ActorSystem()
    override implicit lazy val actorMaterializer: ActorMaterializer = ActorMaterializer()
    override implicit lazy val executionContext: ExecutionContext = actorSystem.dispatcher
  }

  val appConfig = ConfigFactory.load()

  object neo {
    val config = new NeoConfig(appConfig)
    val support = new NeoSupportImpl(config) with RuntimeDepsImpl
  }

  // interpreters instances

  val loggingApi: LoggingApi[Id] = Slf4jLoggingApi
  val loggingInterpreter: LoggingAction ~> TaskEither = new LoggingInterpreter[Id](loggingApi)

  val neoIdGenerationApi: IdGenerationApi[TaskEither] = new NeoIdGenerationApi(neo.support)
  val neoIdGenerationInterpreter: IdGenerationAction ~> TaskEither =
    new IdGenerationInterpreter[TaskEither](neoIdGenerationApi)

  val neoNodeManagementApi: NodeManagementApi[TaskEither] = new NeoNodeManagementApi(neo.support)
  val neoNodeManagementInterpreter: NodeManagementAction ~> TaskEither =
    new NodeManagementInterpreter[TaskEither](neoNodeManagementApi)

}
