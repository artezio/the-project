package io.treev.interpreter

import io.treev.model.db.DbError

package object db {

  /** Database operation base type. */
  type DbTaskEither[+A] = TaskEitherBase[A, DbError] with TaskEither[A]

}
