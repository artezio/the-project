package io.treev.interpreter

import io.treev.interpreter.db._
import io.treev.model.db.Entity
import io.treev.model.nodemanagement.NodeManagementAction._
import io.treev.model.nodemanagement._
import io.treev.support.neo._
import io.treev.support.neo.json._
import io.treev.support.neo.query.{ObjectWriter, ValueWriter, _}
import io.treev.util.tags._
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, Json}

import scala.language.higherKinds
import scalaz.~>

object nodemanagement {

  /** Node management API. */
  trait NodeManagementApi[+F[_]] {
    def create(node: TreeNodeEntity): F[Unit]
    def read(id: TreeNodeEntity#Id): F[Option[TreeNodeEntity]]
  }

  class NeoNodeManagementApi(neoSupport: NeoSupport) extends NodeManagementApi[DbTaskEither] {

    override def create(node: TreeNodeEntity): DbTaskEither[Unit] =
      neoSupport.create(node).unit

    override def read(id: TreeNodeEntity#Id): DbTaskEither[Option[TreeNodeEntity]] =
      ???

    implicit val treeNodeDataWriter: ObjectWriter[TreeNodeData] = (
      write[Long @@ TreeNode]("parentId") ~
      write[String]("content")
    )(unlift(TreeNodeData.unapply))
    implicit val treeNodeEntityWriter: ValueWriter[Entity[Long, TreeNode, TreeNodeData]] = entityWriter(TreeNode.Label)

    implicit val treeNodeDataReads: Format[TreeNodeData] = Json.format[TreeNodeData]
    implicit val treeNodeEntityReads: Format[Entity[Long, TreeNode, TreeNodeData]] = entityFormat(TreeNode.Label)

  }

  /** Node management instructions interpreter. */
  class NodeManagementInterpreter[F[_]](api: NodeManagementApi[F]) extends (NodeManagementAction ~> F) {
    override def apply[A](fa: NodeManagementAction[A]): F[A] =
      (fa match {
        case Create(node) ⇒ api.create(node)
        case Read(id) ⇒ api.read(id)
      }).asInstanceOf[F[A]]
  }

}
