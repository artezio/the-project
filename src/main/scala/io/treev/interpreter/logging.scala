package io.treev.interpreter

import java.io.PrintStream

import io.treev.model.logging.{Level, LoggingAction}
import org.slf4j.LoggerFactory

import scala.language.higherKinds
import scalaz.~>

object logging {

  /** Logging API. */
  trait LoggingApi[+F[_]] {
    def log(msg: String, source: String, level: Level, cause: Option[Throwable] = None): F[Unit]
  }

  /** Console-based logging API implementation. */
  object ConsoleLoggingApi extends LoggingApi[Id] {
    override def log(msg: String, source: String, level: Level, cause: Option[Throwable]): Id[Unit] = {
      val out = getOut(level)
      val formattedMsg = formatMessage(msg, source, level, cause)
      out.println(formattedMsg)
    }

    private def getOut(level: Level): PrintStream =
      if (level == Level.Error) Console.err else Console.out

    private def formatMessage(msg: String, source: String, level: Level, cause: Option[Throwable]): String =
      s"${level.toString} ${source.toUpperCase} - $msg" + {
        cause.fold("")(t ⇒ s" Cause: ${t.getMessage}")
      }
  }

  /** SLF4J-based logging API implementation. */
  object Slf4jLoggingApi extends LoggingApi[Id] {
    override def log(msg: String, source: String, level: Level, cause: Option[Throwable]): Id[Unit] = {
      val logger = LoggerFactory.getLogger(source)

      cause.fold {
        level match {
          case Level.Error ⇒ logger.error(msg)
          case Level.Warn ⇒ logger.warn(msg)
          case Level.Info ⇒ logger.info(msg)
          case Level.Debug ⇒ logger.debug(msg)
        }
      } { cause ⇒
        level match {
          case Level.Error ⇒ logger.error(msg, cause)
          case Level.Warn ⇒ logger.warn(msg, cause)
          case Level.Info ⇒ logger.info(msg, cause)
          case Level.Debug ⇒ logger.debug(msg, cause)
        }
      }
    }
  }

  /** Logging instructions interpreter. */
  class LoggingInterpreter[F[_]](api: LoggingApi[F]) extends (LoggingAction ~> F) {
    override def apply[A](fa: LoggingAction[A]): F[A] =
      api.log(fa.msg, fa.source, fa.level, fa.cause).asInstanceOf[F[A]]
  }

}
