package io.treev

import io.treev.model._

import scala.language.{higherKinds, implicitConversions, reflectiveCalls}
import scalaz._
import scalaz.concurrent.Task

package object interpreter {

  /** Simple operation that has no additional structure around the value it yields. */
  type Id[+A] = A
  /** Asynchronous operation that yields either result of type `A` or an error of type `E`. */
  type TaskEitherBase[+A, +E <: Error] = Task[E \/ A]
  /** Asynchronous operation that yields either result of type `A` or an application error. */
  type TaskEither[+A] = TaskEitherBase[A, Error]

  object implicits {

    /** Monad for `TaskEither` type. */
    implicit object TaskEitherMonad extends Monad[TaskEither] {
      override def point[A](a: ⇒ A): TaskEither[A] =
        Task.now(\/-(a))

      override def bind[A, B](fa: TaskEither[A])(f: A ⇒ TaskEither[B]): TaskEither[B] =
        EitherT(fa).flatMapF(f).run
    }

    /** Convert `Id` to `AsyncEither`. */
    implicit def idToAsyncEither[A, E <: Error](a: Id[A]): TaskEitherBase[A, E] = Task.now(\/-(a))
    /** Convert `Task` to `TaskEither`. */
    implicit def taskToAsyncEither[A, E <: Error](a: Task[A]): TaskEitherBase[A, E] = a.map(\/-(_))

    /** Update natural transformation codomain from `Id` to `AsyncEither`. */
    implicit def codomainIdToAsyncEither[F[_], E <: Error](t: F ~> Id): F ~> ({type f[a] = TaskEitherBase[a, E]})#f =
      new (F ~> ({type f[a] = TaskEitherBase[a, E]})#f) {
        override def apply[A](fa: F[A]): TaskEitherBase[A, E] = idToAsyncEither(t(fa))
      }

    /** Update natural transformation codomain from `Async` to `AsyncEither`. */
    implicit def codomainTaskToTaskEither[F[_], E <: Error](t: F ~> Task): F ~> ({type f[a] = TaskEitherBase[a, E]})#f =
      new (F ~> ({type f[a] = TaskEitherBase[a, E]})#f) {
        override def apply[A](fa: F[A]): TaskEitherBase[A, E] = taskToAsyncEither[A, E](t(fa))
      }

  }

}
