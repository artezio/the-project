package io.treev.interpreter

import io.treev.interpreter.db._
import io.treev.model.idgeneration.IdGenerationAction._
import io.treev.model.idgeneration._
import io.treev.support.neo._

import scala.language.higherKinds
import scalaz.~>

object idgeneration {

  /** Unique identifiers generation API. */
  trait IdGenerationApi[+F[_]] {
    def generateGloballyUnique(): F[Long]
    def generateNamespaceUnique(namespace: String): F[Long]
  }

  /** Fake non-persistent id generation API implementation suited for tests. */
  class FakeIdGenerationApi extends IdGenerationApi[Id] {
    import java.util.concurrent.ConcurrentHashMap
    import java.util.concurrent.atomic.AtomicLong
    import java.util.function.Function

    override def generateGloballyUnique(): Id[Long] = globalCounter.getAndIncrement()

    override def generateNamespaceUnique(namespace: String): Id[Long] = {
      val counter = namespaceCountersMap.computeIfAbsent(namespace, new Function[String, AtomicLong] {
        override def apply(key: String): AtomicLong = new AtomicLong(1)
      })
      counter.getAndIncrement()
    }

    private val globalCounter: AtomicLong = new AtomicLong(1)
    private val namespaceCountersMap: ConcurrentHashMap[String, AtomicLong] = new ConcurrentHashMap[String, AtomicLong]
  }

  /** Neo-based id generation API implementation. */
  class NeoIdGenerationApi(neo: NeoSupport) extends IdGenerationApi[DbTaskEither] {
    import io.treev.support.neo.implicits._

    override def generateGloballyUnique(): DbTaskEither[Long] =
      neo.executeStatement[Long](
        """MERGE (id:GloballyUniqueId)
          |ON CREATE SET id.count = 1
          |ON MATCH SET id.count = id.count + 1
          |RETURN id.count AS id""".stripMargin
      ).firstRow

    override def generateNamespaceUnique(namespace: String): DbTaskEither[Long] =
      neo.executeStatement[Long](
        Statement(
          s"""MERGE (id:UniqueId { name: { namespace } })
             |ON CREATE SET id.count = 1
             |ON MATCH SET id.count = id.count + 1
             |RETURN id.count AS id""".stripMargin,
          Map("namespace" → Parameter.String(namespace))
        )
      ).firstRow

  }

  /** Id generation instructions interpreter. */
  class IdGenerationInterpreter[F[_]](api: IdGenerationApi[F]) extends (IdGenerationAction ~> F) {
    override def apply[A](fa: IdGenerationAction[A]): F[A] =
      (fa match {
        case GenerateGloballyUnique ⇒ api.generateGloballyUnique()
        case GenerateNamespaceUnique(namespace) ⇒ api.generateNamespaceUnique(namespace)
      }).asInstanceOf[F[A]]
  }

}
