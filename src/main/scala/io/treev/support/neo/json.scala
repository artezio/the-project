package io.treev.support.neo

import io.treev.model.db.Entity
import io.treev.util.tags._
import play.api.libs.json._

object json {

  private implicit val parameterWrites: Writes[Parameter] = new Writes[Parameter] {
    override def writes(o: Parameter): JsValue = o match {
      case Parameter.String(value) ⇒ JsString(value)
    }
  }

  private implicit val statementWrites: Writes[Statement] = Json.writes[Statement]
  implicit val neoRequestWrites: Writes[NeoRequest] = Json.writes[NeoRequest]

  private implicit val rowReads: Reads[Row] = Json.reads[Row]
  private implicit val resultReads: Reads[Result] = Json.reads[Result]
  private implicit val neoErrorReads: Reads[NeoError] = Json.reads[NeoError]
  private implicit val neoErrorsReads: Reads[NeoErrors] = Json.reads[NeoErrors]
  implicit val neoResponseReads: Reads[NeoResponse] = Json.reads[NeoResponse]

  val unitReads: Reads[Unit] = new Reads[Unit] {
    override def reads(json: JsValue): JsResult[Unit] =
      JsSuccess(())
  }

  implicit def taggedValueFormat[T, U](implicit tFormat: Format[T]): Format[T @@ U] = new Format[T @@ U] {
    override def writes(o: @@[T, U]): JsValue = tFormat.writes(o)
    override def reads(json: JsValue): JsResult[T @@ U] = tFormat.reads(json).map(_.@@[U])
  }

  def entityFormat[I, T, D](label: String @@ T)
                           (implicit idFormat: Format[I @@ T], dataFormat: Format[D]): Format[Entity[I, T, D]] =
    new Format[Entity[I, T, D]] {
      override def writes(entity: Entity[I, T, D]): JsValue =
        dataFormat.writes(entity.data).as[JsObject] ++ Json.obj(fields.Id → idFormat.writes(entity.id))

      override def reads(json: JsValue): JsResult[Entity[I, T, D]] = json match {
        case JsObject(map) ⇒
          map.get(fields.Id).map { idJson ⇒
            for {
              id ← idFormat.reads(idJson)
              data ← dataFormat.reads(json)
            } yield Entity[I, T, D](label, id, data)
          } getOrElse JsError(s"Cannot read id from response: ${json.toString}")
        case _ ⇒
          JsError(s"Cannot read entity from response: ${json.toString}")
      }
    }

}
