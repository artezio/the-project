package io.treev.support

import io.treev.interpreter.db.DbTaskEither
import io.treev.model.db._
import play.api.data.validation.ValidationError
import play.api.libs.json._

import scala.language.implicitConversions
import scala.util.control.NonFatal
import scalaz.{-\/, EitherT}

package object neo {

  /** Neo request. */
  case class NeoRequest(statements: List[Statement])

  /** Neo statement. */
  case class Statement(statement: String, parameters: Map[String, Parameter] = Map.empty)
  object Statement {
    implicit def stringToStatement(statement: String): Statement = Statement(statement)
  }

  /** Base query parameter type. */
  sealed trait Parameter
  object Parameter {
    case class String(value: java.lang.String) extends Parameter
  }

  /** Neo response. */
  case class NeoResponse(results: List[Result], errors: List[NeoError])
  case class Result(columns: List[String], data: List[Row])
  case class Row(row: JsArray)

  case class NeoError(code: String, message: String)
  case class NeoErrors(errors: List[NeoError]) extends DbError

  case class ResponseParseError(errors: Seq[(JsPath, Seq[ValidationError])]) extends DbProgrammingError
  case object HeadOfEmptyResultError extends DbProgrammingError

  /** Encapsulates Neo statement execution result with error handling and additional helpers. */
  case class ExecutionResult[A](private val task: DbTaskEither[List[List[A]]]) {
    /** Execution results. */
    def results: DbTaskEither[List[List[A]]] = task.handle {
      case NonFatal(t) ⇒ -\/(GenericDbError(t))
    }

    /** First result rows. */
    def firstResult: DbTaskEither[List[A]] = head(results)
    /** First row of the first result. */
    def firstRow: DbTaskEither[A] = head(firstResult)

    /** Convert to empty execution result. */
    def unit: DbTaskEither[Unit] = EitherT(task).map(_ ⇒ ()).run

    private def head[B](value: DbTaskEither[List[B]]): DbTaskEither[B] =
      EitherT(value).map(_.head).run.handle {
        case e: NoSuchElementException ⇒ -\/(HeadOfEmptyResultError)
      }
  }

  object implicits {

    implicit def neoResponseReads[A](implicit aReads: Reads[A]): Reads[A] =
      new Reads[A] {
        override def reads(json: JsValue): JsResult[A] = json match {
          case JsArray(values) ⇒
            values.headOption
              .map(value ⇒ Json.fromJson[A](value)(aReads))
              .getOrElse(error(json))
          case _ ⇒
            error(json)
        }

        private def error(json: JsValue): JsError =
          JsError(s"Cannot read single value from response: ${json.toString}")
      }

  }

}
