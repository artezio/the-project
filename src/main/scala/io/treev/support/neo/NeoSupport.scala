package io.treev.support.neo

import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl._
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import io.treev.interpreter.db._
import io.treev.model.db.Entity
import io.treev.runtime.RuntimeDeps
import io.treev.support.neo.json.unitReads
import io.treev.support.neo.query.ValueWriter
import play.api.libs.json._

import scala.language.postfixOps

/** Neo support. */
trait NeoSupport {
  /** Execute multiple statements. */
  def executeStatements[A](statements: List[Statement])
                          (implicit resultReads: Reads[A]): ExecutionResult[A]
  /** Execute single statement. */
  def executeStatement[A](statement: Statement)
                         (implicit resultReads: Reads[A]): ExecutionResult[A]
  /** Execute statement without result parsing. */
  def executeStatementNoResult(statement: Statement): ExecutionResult[Unit]

  /** Persist entity using 'create' statement. */
  def create[I, T, D](entity: Entity[I, T, D])
                    (implicit entityWriter: ValueWriter[Entity[I, T, D]]): ExecutionResult[Unit]
}

class NeoSupportImpl(neoConfig: NeoConfig) extends NeoSupport {
  this: RuntimeDeps ⇒

  import PlayJsonSupport._
  import io.treev.util.concurrency._

  override def executeStatements[A](statements: List[Statement])
                                   (implicit resultReads: Reads[A]): ExecutionResult[A] = {
    val request = createRequest(neoUtil.commitUrl, statements)
    ExecutionResult(executeRequest(request))
  }

  override def executeStatement[A](statement: Statement)
                                  (implicit resultReads: Reads[A]): ExecutionResult[A] =
    executeStatements(List(statement))

  override def executeStatementNoResult(statement: Statement): ExecutionResult[Unit] =
    executeStatement[Unit](statement)(unitReads)

  override def create[I, T, D](entity: Entity[I, T, D])
                              (implicit entityWriter: ValueWriter[Entity[I, T, D]]): ExecutionResult[Unit] =
    executeStatementNoResult {
      s"CREATE (:${entity.label} ${entityWriter.write(entity).value})"
    }

  private val neoUtil: NeoUtil = new NeoUtil(neoConfig)

  private lazy val connection =
    Http().outgoingConnection(neoConfig.host, neoConfig.port)

  private def createRequest(url: String, statements: List[Statement]): HttpRequest =
    RequestBuilding.Post(url, neoUtil.createRequest(statements))

  private def executeRequest[A](request: HttpRequest)
                               (implicit resultReads: Reads[A]): DbTaskEither[List[List[A]]] = task {
    import io.treev.support.neo.json._

    Source.single(request).via(connection).runWith(Sink.head)
      .flatMap(httpResponse ⇒ Unmarshal(httpResponse).to[NeoResponse])
      .map(neoUtil.readResponse[A])
  }

}
