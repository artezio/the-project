package io.treev.support.neo

import com.typesafe.config.Config

class NeoConfig(config: Config) {

  lazy val host: String = neoConfig.getString("host")
  lazy val port: Int = neoConfig.getInt("port")

  private val neoConfig: Config = config.getConfig("neo")

}
