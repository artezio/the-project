package io.treev.support.neo

import io.treev.model.db.DbError
import io.treev.support.neo.json._
import play.api.libs.json._

import scala.language.postfixOps
import scalaz.{-\/, \/, \/-}

class NeoUtil(neoConfig: NeoConfig) {

  val baseUrl: String = s"http://${neoConfig.host}:${neoConfig.port}"

  val commitUrl: String = baseUrl + "/db/data/transaction/commit"

  /** Create request with supplied statements. */
  def createRequest(statements: List[Statement]): JsValue =
    Json.toJson(NeoRequest(statements))

  /** Read response from Neo response.
    * @tparam A expected row type */
  def readResponse[A](response: NeoResponse)
                     (implicit resultReads: Reads[A]): DbError \/ List[List[A]] =
    if (response.errors.isEmpty) {
      val results = response.results.map { result ⇒
        result.data.map { data ⇒
          resultReads.reads(data.row)
        }
      }

      val parseErrors = results.view.flatten.collect({ case JsError(e) ⇒ e })

      if (parseErrors.isEmpty) \/-(results.map(_.collect({ case JsSuccess(a, _) ⇒ a })))
      else -\/(ResponseParseError(parseErrors.flatten.toList))

    } else -\/(NeoErrors(response.errors))

}
