package io.treev.support.neo

import io.treev.model.db.Entity
import io.treev.util.tags.@@
import play.api.libs.functional._

object query {

  /** Base query value. */
  sealed trait Value {
    def value: String
  }

  /** Single-valued query value. */
  trait SingleValue extends Value
  object SingleValue {
    def apply(_value: String): SingleValue = new SingleValue {
      override def value: String = _value
    }
  }

  /** Object query value. */
  trait ObjectValue extends SingleValue {
    def values: Map[String, String]
  }
  object ObjectValue {
    def apply(_values: Map[String, String]): ObjectValue = new ObjectValue {
      override def values: Map[String, String] = _values
      override def value: String = foldValues(values)
    }

    def foldValues(values: Map[String, String]): String =
      s"{ ${values.view.map(p ⇒ s"${p._1} : ${p._2}").mkString(", ")} }"
  }

  /** Single value writer. */
  trait ValueWriter[-T] {
    def write(value: T): SingleValue
  }
  object ValueWriter {
    implicit object stringValueWriter extends ValueWriter[String] {
      override def write(value: String): SingleValue = SingleValue(s"'$value'")
    }
    implicit object intValueWriter extends ValueWriter[Int] {
      override def write(value: Int): SingleValue = SingleValue(value.toString)
    }
    implicit object longValueWriter extends ValueWriter[Long] {
      override def write(value: Long): SingleValue = SingleValue(value.toString)
    }
    implicit object doubleValueWriter extends ValueWriter[Double] {
      override def write(value: Double): SingleValue = SingleValue(value.toString)
    }
    implicit object booleanValueWriter extends ValueWriter[Boolean] {
      override def write(value: Boolean): SingleValue = SingleValue(value.toString)
    }
  }

  /** Object (multiple values) writer. */
  trait ObjectWriter[-T] extends ValueWriter[T] {
    override def write(value: T): ObjectValue
  }
  object ObjectWriter {
    implicit object ObjectWriterContravariant extends ContravariantFunctor[ObjectWriter] {
      override def contramap[A, B](m: ObjectWriter[A], f: B ⇒ A): ObjectWriter[B] =
        new ObjectWriter[B] {
          override def write(value: B): ObjectValue = m.write(f(value))
        }
    }
  }

  implicit object ObjectWriterFunctionalCanBuild extends FunctionalCanBuild[ObjectWriter] {
    override def apply[A, B](ma: ObjectWriter[A], mb: ObjectWriter[B]): ObjectWriter[A ~ B] =
      new ObjectWriter[A ~ B] {
        override def write(value: A ~ B): ObjectValue =
          ObjectValue(ma.write(value._1).values ++ mb.write(value._2).values)
      }
  }

  def write[T](name: String)(implicit valueWriter: ValueWriter[T]): ObjectWriter[T] =
    new ObjectWriter[T] {
      override def write(value: T): ObjectValue = ObjectValue(Map(name → valueWriter.write(value).value))
    }

  /** Create entity writer. */
  def entityWriter[I, T, D](label: String @@ T)
                           (implicit idWriter: ValueWriter[I @@ T],
                                     dataWriter: ObjectWriter[D]): ValueWriter[Entity[I, T, D]] =
    new ValueWriter[Entity[I, T, D]] {
      override def write(value: Entity[I, T, D]): SingleValue =
        ObjectValue(dataWriter.write(value.data).values + ("id" → idWriter.write(value.id).value))
    }

}
