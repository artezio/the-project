import io.treev.model._, logging._
import io.treev.model.idgeneration.IdGenerationActions
import io.treev.util._, mechanics._, product._
import scala.language.{existentials, higherKinds, implicitConversions, reflectiveCalls}
import scalaz._, concurrent._, Id.Id

object playground extends App {

  object model {
    type Async[+A] = Task[Error \/ A]

    object asyncEitherResultMonad extends Monad[Async] {
      override def bind[A, B](fa: Async[A])(f: A ⇒ Async[B]): Async[B] =
        EitherT(fa).flatMapF(f).run
      override def point[A](a: ⇒ A): Async[A] =
        Task(\/-(a))
    }

    object testing {
      case class Step[A](action: Action[A], result: A, state: Any)
      case class Trace(steps: Vector[Step[_]] = Vector.empty) {
        def next(step: Step[_]): Trace = this.copy(steps = steps :+ step)
      }

      object tracing {
        type TraceTarget[S] = {
          type f[A] = State[S, A]
        }
        trait Tracer[F[_] <: Action[_], S] extends (F ~> TraceTarget[S]#f)

        def trace[S, A](s: S, action: Action[A], result: A)(implicit traceUpdater: ProductUpdater[S, Trace]): S =
          update[S, Trace](s)(_.next(Step(action, result, s)))
      }
    }

    object logging {
      object impl {
        type ConsoleT = LoggingAction ~> Id
        object Console extends ConsoleT {
          override def apply[A](action: LoggingAction[A]): Id[A] =
            action match {
              case _: LoggingAction.Error |
                   _: LoggingAction.Warn |
                   _: LoggingAction.Info |
                   _: LoggingAction.Debug ⇒
                println(s"${action.level.entryName}: ${action.msg}")
            }
        }

        type AsyncEitherConsoleT = LoggingAction ~> Async
        object AsyncEitherConsole extends AsyncEitherConsoleT {
          override def apply[A](action: LoggingAction[A]): Async[A] =
            asyncEitherResultMonad.point(Console[A](action))
        }

        import testing._, tracing._
        case class LoggingTracer[S](implicit traceUpdater: ProductUpdater[S, Trace]) extends Tracer[LoggingAction, S] {
          override def apply[A](action: LoggingAction[A]): State[S, A] = State { s ⇒
            val result = Console[A](action)
            (trace(s, action, result), result)
          }
        }
      }
    }

    object db {
      import io.treev.util.tags._

      sealed trait DbAction[A] extends Action[A]

      trait Entity[I, +D] {
        def id: I @@ D
        def data: D
      }
      object Entity {
        def apply[I, D](_id: I @@ D, _data: D): Entity[I, D] = new Entity[I, D] {
          override def id: I @@ D = _id
          override def data: D = _data
        }
      }

      object DbAction {
        case class Persist[I, D](entity: Entity[I, D]) extends DbAction[Entity[I, D]]
        case class Read[I, D](id: I @@ D) extends DbAction[Option[Entity[I, D]]]
        case class Delete[I, D](id: I @@ D) extends DbAction[Boolean]
      }

      class DbActions[F[_]](implicit I: Inject[DbAction, F]) {
        import DbAction._
        def persist[I, D](entity: Entity[I, D]): Free[F, Entity[I, D]] = lift(Persist(entity))
        def persist[I, D](id: I @@ D, data: D): Free[F, Entity[I, D]] = persist(Entity(id, data))
        def read[I, D](id: I @@ D): Free[F, Option[Entity[I, D]]] = lift(Read(id))
        def delete[I, D](id: I @@ D): Free[F, Boolean] = lift(Delete(id))
      }
      object DbActions {
        implicit def instance[F[_]](implicit I: Inject[DbAction, F]): DbActions[F] = new DbActions[F]
      }

      object impl {
        type Db = java.util.concurrent.ConcurrentHashMap[AnyRef, Entity[Any, Any]]

        def emptyDb: Db = new java.util.concurrent.ConcurrentHashMap[AnyRef, Entity[Any, Any]]

        type InMemoryT = DbAction ~> Id
        case class InMemory(db: Db = emptyDb) extends InMemoryT {
          override def apply[A](action: DbAction[A]): Id[A] = action match {
            case DbAction.Persist(entity) ⇒
              db.put(entity.id, entity)
              entity
            case DbAction.Read(id) ⇒
              Option(db.get(id))
            case DbAction.Delete(id) ⇒
              Option(db.remove(id)).isDefined
          }
        }

        type AsyncEitherInMemoryT = DbAction ~> Async
        case class AsyncEitherInMemory(db: Db = emptyDb) extends AsyncEitherInMemoryT {
          override def apply[A](action: DbAction[A]): Task[Error \/ A] =
            asyncEitherResultMonad.point(InMemory(db)(action))
        }

        import testing._, tracing._
        case class DbTracer[S](db: Db = emptyDb)
                              (implicit traceUpdater: ProductUpdater[S, Trace])
          extends Tracer[DbAction, S] {

          override def apply[A](action: DbAction[A]): State[S, A] = State { s ⇒
            val result = InMemory(db)(action)
            (trace(s, action, result), result)
          }
        }
      }
    }
  }

  object program {
    import io.treev.util.tags._
    import model._, db._

    case class User(email: String @@ UserData, override val data: UserData) extends Entity[String, UserData] {
      override def id: String @@ UserData = email
    }
    case class UserData(name: String)

    def persistAndDeleteUser[F[_]](user: User)
                                  (implicit LoggingActions: LoggingActions[F],
                                            DbActions: DbActions[F]): Free[F, Unit] = {
      implicit val loggingSource = "UserService".@@[Logging]

      for {
        _ ← LoggingActions.info(s"Persisting user ${user.data.name}")
        persistedUser ← DbActions.persist(user)
        _ ← LoggingActions.debug(s"Persisted user ${user.data.name}")

        _ ← LoggingActions.info(s"Deleting user ${user.data.name}")
        deleted ← DbActions.delete(persistedUser.id)
        _ ← LoggingActions.debug {
          if (deleted) s"Deleted user ${user.data.name}" else s"Cannot delete user ${user.data.name}"
        }
      } yield ()
    }

    val user = User("scooper@example.org".@@[UserData], UserData("Sheldon Cooper"))

    type PRG[A] = Coproduct[LoggingAction, DbAction, A]

    def runId() = {
      val interpreter: PRG ~> Id = logging.impl.Console or db.impl.InMemory()
      val app: Free[PRG, Unit] = persistAndDeleteUser[PRG](user)

      app.foldMap(interpreter)
    }

    def runAsyncEither() = {
      val interpreter: PRG ~> Async = logging.impl.AsyncEitherConsole or db.impl.AsyncEitherInMemory()
      val app: Free[PRG, Unit] = persistAndDeleteUser[PRG](user)

      app.foldMap(interpreter)(asyncEitherResultMonad).runFor(1000)
    }

    import testing._, tracing._
    def runStateful() = {
      case class Env()
      type S = Trace & Env

      val interpreter: PRG ~> TraceTarget[S]#f =
        NaturalTransformationOrPimp[LoggingAction, TraceTarget[S]#f](logging.impl.LoggingTracer[S]()) or db.impl.DbTracer[S]()
      val app: Free[PRG, Unit] = persistAndDeleteUser[PRG](user)

      app.foldMap[TraceTarget[S]#f](interpreter).run(Trace() & Env())
    }

    def runApp() = {
      import io.treev.app._
      import io.treev.interpreter._
      import io.treev.interpreter.implicits._

      interpret[IS, TaskEither](taskEitherInterpreter).runFor(1000)
    }
  }

  /*println(program.runId())
  println(program.runAsyncEither())
  println(program.runStateful())*/
  println(program.runApp())
}
