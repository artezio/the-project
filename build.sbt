scalaVersion := "2.11.7"

name := "The Project"

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

val scalazVersion = "7.2.0-M4"

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core" % scalazVersion,
  "org.scalaz" %% "scalaz-concurrent" % scalazVersion,
  "com.chuusai" %% "shapeless" % "2.2.5",
  "com.beachape" %% "enumeratum" % "1.3.2",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.play" %% "play-json" % "2.4.3",
  "com.typesafe" % "config" % "1.3.0",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.0-M1",
  "de.heikoseeberger" %% "akka-http-play-json" % "1.2.0"
)
